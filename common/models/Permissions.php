<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "permissions".
 *
 * @property int $id
 * @property string $title
 * @property string $perm_level
 */
class Permissions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'permissions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'perm_level'], 'required'],
            [['title'], 'string', 'max' => 20],
            [['perm_level'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'perm_level' => 'Perm Level',
        ];
    }
    
    public function getPermission(){
//        return static::find()->select("title")->;
    }
}
