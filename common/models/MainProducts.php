<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "main_products".
 *
 * @property int $id
 * @property int $product_id
 * @property int $unique_product_id
 * @property int $category_id
 * @property string $time
 *
 * @property Products $product
 * @property UniqueProducts $uniqueProduct
 * @property ProductCategories $category
 */
class MainProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'unique_product_id', 'category_id'], 'required'],
            [['product_id', 'unique_product_id', 'category_id'], 'integer'],
            [['time'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['unique_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => UniqueProducts::className(), 'targetAttribute' => ['unique_product_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'unique_product_id' => Yii::t('app', 'Unique Product ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniqueProduct()
    {
        return $this->hasOne(UniqueProducts::className(), ['id' => 'unique_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategories::className(), ['id' => 'category_id']);
    }
    
    public function findById($id){
        return self::find()->where(['id'=>$id])->one();
    }
}
