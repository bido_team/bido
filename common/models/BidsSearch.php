<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bids;

/**
 * BidsSearch represents the model behind the search form of `common\models\Bids`.
 */
class BidsSearch extends Bids
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bid_id', 'user_id', 'order_id'], 'integer'],
            [['date_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bids::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bid_id' => $this->bid_id,
            'user_id' => $this->user_id,
            'order_id' => $this->order_id,
            'date_time' => $this->date_time,
        ]);

        return $dataProvider;
    }
}
