<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unique_products".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $img
 * @property int $category_id
 *
 * @property Products[] $products
 * @property ProductCategories $category
 * @property UniqueProductsTrans[] $uniqueProductsTrans
 */
class UniqueProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unique_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'img', 'category_id'], 'required'],
            [['description', 'img'], 'string'],
            [['category_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'img' => Yii::t('app', 'Img'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['unique_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniqueProductsTrans()
    {
        return $this->hasMany(UniqueProductsTrans::className(), ['unique_product_id' => 'id']);
    }
}
