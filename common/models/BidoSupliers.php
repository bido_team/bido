<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bido_supliers".
 *
 * @property int $id
 * @property string $title
 * @property double $rating
 * @property string $description
 * @property string $img
 * @property int $unique_id
 * @property string $email
 * @property int $number
 * @property string $adress
 */
class BidoSupliers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bido_supliers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'rating', 'description', 'img', 'unique_id', 'email', 'number', 'adress'], 'required'],
            [['rating'], 'number'],
            [['description', 'img', 'adress'], 'string'],
            [['unique_id', 'number'], 'integer'],
            [['title', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'rating' => Yii::t('app', 'Rating'),
            'description' => Yii::t('app', 'Description'),
            'img' => Yii::t('app', 'Img'),
            'unique_id' => Yii::t('app', 'Unique ID'),
            'email' => Yii::t('app', 'Email'),
            'number' => Yii::t('app', 'Number'),
            'adress' => Yii::t('app', 'Adress'),
        ];
    }
    
    public function getSuplierDataByUserId($user_id){
        if(!empty($user_id)){
            $suplier_data = Yii::$app->db->createCommand(
                "SELECT bs.* FROM `bido_supliers` AS bs 
                LEFT OUTER JOIN `user2suplier` AS u2s ON u2s.user_id = :id
                WHERE bs.id = u2s.suplier_id;")
            ->bindParam(':id', $user_id)
            ->queryOne();
            return $suplier_data;
        }
        return false;
    }
    
    public function getUser2suplier()
    {
        return $this->hasMany(User2suplier::className(), ['suplier_id' => 'id']);
    }
    
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['suplier_id' => 'id']);
    }
}
