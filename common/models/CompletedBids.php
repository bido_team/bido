<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "completed_orders".
 *
 * @property int $id
 * @property int $bid_id
 * @property int $status
 * @property string $time
 */
class CompletedBids extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'completed_bids';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'bid_id', 'status'], 'required'],
            [[ 'bid_id', 'status'], 'integer'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'bid_id' => Yii::t('app', 'Bid ID'),
            'status' => Yii::t('app', 'Status'),
            'time' => Yii::t('app', 'Time'),
        ];
    }
    
    public function countCompletedBidsBySuplierId($suplier_id){
        if(!empty($suplier_id)){
            $completed_bid_count = Yii::$app->db->createCommand(
                "SELECT COUNT(co.id) as completed_bid_numb FROM `user2suplier` AS u2s
                LEFT OUTER JOIN `bids` AS b ON b.user_id = u2s.user_id
                LEFT JOIN `completed_bids` AS co ON co.bid_id = b.bid_id AND co.status = 1
                WHERE u2s.suplier_id = :id;")
            ->bindParam(':id', $suplier_id)
            ->queryOne();
            return $completed_bid_count['completed_bid_numb'];
        }
        return false;
    }
    
    public function countCompletedBidsByUserId($user_id){
        if(!empty($user_id)){
            $completed_bid_count = Yii::$app->db->createCommand(
               "SELECT COUNT(co.id) as completed_bid_numb FROM `bids` as b
                INNER JOIN `completed_bids` AS co ON co.bid_id = b.bid_id AND co.status = 1
                WHERE b.user_id = :id;")
            ->bindParam(':id', $user_id)
            ->queryOne();
            return $completed_bid_count['completed_bid_numb'];
        }
        return false;
    }
}
