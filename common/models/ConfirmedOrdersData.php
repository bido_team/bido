<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "confirmed_order_data".
 *
 * @property int $id
 * @property int $confirmed_order_id
 * @property int $price
 * @property int $delivery_time
 * @property int $guaranty
 *
 * @property ConfirmedOrders $confirmedOrder
 */
class ConfirmedOrdersData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'confirmed_order_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['confirmed_order_id', 'price', 'delivery_time', 'guaranty'], 'required'],
            [['confirmed_order_id', 'price', 'delivery_time', 'guaranty'], 'integer'],
            [['confirmed_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConfirmedOrders::className(), 'targetAttribute' => ['confirmed_order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'confirmed_order_id' => Yii::t('app', 'Confirmed Order ID'),
            'price' => Yii::t('app', 'Price'),
            'delivery_time' => Yii::t('app', 'Delivery Time'),
            'guaranty' => Yii::t('app', 'Guaranty'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedOrder()
    {
        return $this->hasOne(ConfirmedOrders::className(), ['id' => 'confirmed_order_id']);
    }
}
