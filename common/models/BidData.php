<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bid_data".
 *
 * @property int $bid_id
 * @property int $price
 * @property string $currency
 * @property string $delivery_time
 * @property string $guaranty
 * @property string $date_time
 */
class BidData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bid_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bid_id', 'price', 'currency', 'delivery_time', 'guaranty'], 'required'],
            [['bid_id', 'price'], 'integer'],
            [['date_time'], 'safe'],
            [['currency'], 'string', 'max' => 200],
            [['delivery_time', 'guaranty'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bid_id' => Yii::t('app', 'Bid ID'),
            'price' => Yii::t('app', 'Price'),
            'currency' => Yii::t('app', 'Currency'),
            'delivery_time' => Yii::t('app', 'Delivery Time'),
            'guaranty' => Yii::t('app', 'Guaranty'),
            'date_time' => Yii::t('app', 'Date Time'),
        ];
    }
    
    public function getBids()
    {
        return $this->hasOne(Bids::className(), ['bid_id' => 'bid_id']);
    }
}
