<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $repeat_password;

    const SCENARIO_PASS_CONFIRM = 'pass_confirm';
    const SCENARIO_DEFAULT = 'default';
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            [['email', 'username'], 'trim'],
            [['email', 'username', 'password'], 'required'],
            ['repeat_password', 'required', 'on' => self::SCENARIO_PASS_CONFIRM],
            [['repeat_password','password'], 'confirmRetypedPassword', 'on' => self::SCENARIO_PASS_CONFIRM],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'string', 'min' => 6],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'repeat_password' => Yii::t('app','Repeat Password'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
    
    public function confirmRetypedPassword($attribute, $params){
        $attribute_labels = $this->attributeLabels();
        $label = $attribute_labels['repeat_password'];
        if($this->password  !== $this->repeat_password){
            $this->addError('repeat_password', Yii::t("app","Passwords don't match."));
        }
    }
}
