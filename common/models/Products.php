<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $suplier_id
 * @property double $price
 * @property int $amount
 * @property int $delivery_time
 * @property int $guaranty
 * @property int $unique_product_id
 * @property string $time
 *
 * @property ConfirmedOrders[] $confirmedOrders
 * @property Supliers $suplier
 * @property UniqueProducts $uniqueProduct
 */
class Products extends \yii\db\ActiveRecord
{
    
    private $_orderNumb;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['suplier_id', 'price', 'amount', 'delivery_time', 'guaranty', 'unique_product_id', 'time'], 'required'],
            [['suplier_id', 'amount', 'delivery_time', 'guaranty', 'unique_product_id'], 'integer'],
            [['price'], 'number'],
            [['time'], 'safe'],
            [['suplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supliers::className(), 'targetAttribute' => ['suplier_id' => 'id']],
            [['unique_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => UniqueProducts::className(), 'targetAttribute' => ['unique_product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'suplier_id' => Yii::t('app', 'Suplier ID'),
            'price' => Yii::t('app', 'Price'),
            'amount' => Yii::t('app', 'Amount'),
            'delivery_time' => Yii::t('app', 'Delivery Time'),
            'guaranty' => Yii::t('app', 'Guaranty'),
            'unique_product_id' => Yii::t('app', 'Unique Product ID'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedOrders()
    {
        return $this->hasMany(ConfirmedOrders::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuplier()
    {
        return $this->hasOne(Supliers::className(), ['id' => 'suplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniqueProduct()
    {
        return $this->hasOne(UniqueProducts::className(), ['id' => 'unique_product_id']);
    }

     public function getOrderNumb(){
         return $this->_orderNumb;
     }

     public function setOrderNumb($orderNumb){
         $this->_orderNumb = $orderNumb;
     }
    public function getMainProducts($category_id)
    {
        $customers = [];
        if(!empty($category_id)){
            $customers = Products::find()
            ->joinWith('productInfo')
            ->select('bido_products.*,product_info.description,product_info.img')
            ->leftJoin('bido_main_products', '`bido_main_products`.`product_id` = `bido_products`.`id`')
            ->leftJoin('bido_product_types', '`bido_product_types`.`product_id` = `bido_main_products`.`product_type_id`')
            ->where(['bido_main_products.category' => $category_id])
            ->all();
        }
        return $customers;
    }
    
    public function getProductData($id)
    {
        $P_data = null;
        if(!empty($id)){
            $P_data = Products::find()
            ->joinWith('productInfo')
            ->select('*')
            ->where(['bido_products.id' => $id])
            ->one();
        }
        return $P_data;
    }
    
    public function getProductsBySpecificIds($ids = [])
    {
        if(!empty($ids) && is_array($ids)){
            $products = self::find()
            ->select('*')
            ->where(['in', 'id', $ids])
            ->all();
        }
        return (isset($products)) ? $products : null;
    }
}
