<?php

namespace common\models;

use Yii;
//use common\models\ConfirmedOrdersData;

/**
 * This is the model class for table "confirmed_orders".
 *
 * @property int $id
 * @property int $product_id
 * @property int $order_amount
 * @property int $user_id
 * @property string $time
 *
 * @property ConfirmedOrderData[] $confirmedOrderDatas
 * @property Products $product
 * @property User $user
 */
class ConfirmedOrders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'confirmed_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'order_amount', 'time'], 'required'],
            [['product_id', 'order_amount', 'user_id' ,'unique_product_id'], 'integer'],
            [['time'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'unique_product_id' => Yii::t('app', 'Unique Product ID'),
            'order_amount' => Yii::t('app', 'Order Amount'),
            'user_id' => Yii::t('app', 'User ID'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedOrdersData()
    {
        return $this->hasOne(ConfirmedOrdersData::className(), ['confirmed_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniqueProduct()
    {
        return $this->hasOne(UniqueProducts::className(), ['id' => 'unique_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function countAvailableBidsBySuplierId($suplier_id){
        if(!empty($suplier_id)){
            $available_bid_count = Yii::$app->db->createCommand(
                "SELECT COUNT(co.id) as available_bid_numb FROM `confirmed_orders` AS co 
                LEFT OUTER JOIN `bido_products` AS p1 ON p1.suplier_id = :id
                INNER JOIN `bido_products` AS p2 ON p2.id = co.product_id AND p2.category = p1.category;")
            ->bindParam(':id', $suplier_id)
            ->queryOne();
            return $available_bid_count['available_bid_numb'];
        }
        return false;
    }
    
    public function countAvailableBidsByUserId($user_id){
        if(!empty($user_id)){
            $available_bid_count = Yii::$app->db->createCommand(
                "SELECT COUNT(co.id) as available_bid_numb FROM `confirmed_orders` AS co 
                LEFT OUTER JOIN `user2suplier` AS usc ON usc.user_id = :id
                LEFT OUTER JOIN `bido_products` AS p1 ON p1.suplier_id = usc.suplier_id
                INNER JOIN `bido_products` AS p2 ON p2.id = co.product_id AND p2.category = p1.category;")
            ->bindParam(':id', $user_id)
            ->queryOne();
            return $available_bid_count['available_bid_numb'];
        }
        return false;
    }
    
    public function getUserComfirmedOrders()
    {
        $user = Yii::$app->user->identity;
        if(empty($user)){
            return [];
        }
        return self::find()
            ->select('*')
            ->where(['=', 'user_id', $user->id])
            ->all();
    }
    
    public function countUserComfirmedOrders()
    {
        $user = Yii::$app->user->identity;
        $count = self::find()
            ->select('COUNT(id) as id')
            ->where(['=', 'user_id', $user->id])
            ->all();
        return $count[0]->id;
    }
}
