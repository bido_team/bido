<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

/**
 * ProductsSearch represents the model behind the search form of `common\models\Products`.
 */
class ProductsSearch extends Products
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'suplier_id', 'amount', 'delivery_time', 'guaranty', 'unique_product_id'], 'integer'],
            [['price'], 'number'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ids = [])
    {
        $query = Products::find()->joinWith('uniqueProduct', true, 'LEFT OUTER JOIN');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'products.id' => $this->id,
            'products.suplier_id' => $this->suplier_id,
            'products.price' => $this->price,
            'products.amount' => $this->amount,
            'products.delivery_time' => $this->delivery_time,
            'products.guaranty' => $this->guaranty,
            'products.unique_product_id' => $this->unique_product_id,
            'products.time' => $this->time,
        ]);
        
        if(!empty($ids)){
            $query->andFilterWhere([
                'in', 'products.id', $ids
            ]);
        }

        return $dataProvider;
    }
}
