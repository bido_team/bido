<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_categories".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $img
 *
 * @property MainProducts[] $mainProducts
 * @property ProductCategoriesTrans[] $productCategoriesTrans
 * @property UniqueProducts[] $uniqueProducts
 */
class ProductCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'img'], 'required'],
            [['description', 'img'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'img' => Yii::t('app', 'Img'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainProducts()
    {
        return $this->hasMany(MainProducts::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategoriesTrans()
    {
        return $this->hasMany(ProductCategoriesTrans::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniqueProducts()
    {
        return $this->hasMany(UniqueProducts::className(), ['category_id' => 'id']);
    }
}
