<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "supliers".
 *
 * @property int $id
 * @property string $title
 * @property double $rating
 * @property string $description
 * @property string $img
 * @property string $email
 * @property int $phone_number
 * @property string $adress
 *
 * @property Products[] $products
 */
class Supliers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supliers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['rating'], 'number'],
            [['description', 'img', 'adress'], 'string'],
            [['phone_number'], 'integer'],
            [['title', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'rating' => Yii::t('app', 'Rating'),
            'description' => Yii::t('app', 'Description'),
            'img' => Yii::t('app', 'Img'),
            'email' => Yii::t('app', 'Email'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'adress' => Yii::t('app', 'Adress'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['suplier_id' => 'id']);
    }
}
