<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user2suplier".
 *
 * @property int $id
 * @property int $user_id
 * @property int $suplier_id
 */
class User2suplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user2suplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'suplier_id'], 'required'],
            [['user_id', 'suplier_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'suplier_id' => Yii::t('app', 'Suplier ID'),
        ];
    }
    
    public function getBids()
    {
        return $this->hasOne(Bids::className(), ['user_id' => 'user_id']);
    }
    
    public function getBidoSupliers()
    {
        return $this->hasOne(BidoSupliers::className(), ['id' => 'suplier_id']);
    }
}
