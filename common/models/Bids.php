<?php

namespace common\models;

use Yii;
use yii\db\Connection;


/**
 * This is the model class for table "bids".
 *
 * @property int $bid_id
 * @property int $user_id
 * @property int $order_id
 * @property string $date_time
 */
class Bids extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bids';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id'], 'required'],
            [['user_id', 'order_id'], 'integer'],
            [['date_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bid_id' => Yii::t('app', 'Bid ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'date_time' => Yii::t('app', 'Date Time'),
        ];
    }
    
    public function countBidsBySuplierId($suplier_id){
        if(!empty($suplier_id)){
            $bid_count = Yii::$app->db->createCommand(
                "SELECT COUNT(b.bid_id) as bid_numb FROM `user2suplier` AS u2s
                LEFT OUTER JOIN `bids` AS b ON u2s.user_id = b.user_id
                WHERE u2s.suplier_id = :id;")
            ->bindParam(':id', $suplier_id)
            ->queryOne();
            return $bid_count['bid_numb'];
        }
        return false;
    }
    
    public function countBidsByUserId($user_id){
        if(!empty($user_id)){
            $bid_count = Yii::$app->db->createCommand(
                "SELECT COUNT(bid_id) as bid_numb FROM `bids`
                WHERE user_id = :id;")
            ->bindParam(':id', $user_id)
            ->queryOne();
            return $bid_count['bid_numb'];
        }
        return false;
    }
    
    
    public function getConfirmedOrders(){
        return $this->hasOne(ConfirmedOrders::className(), ['id' => 'order_id']);
    }
    
    
    public function getBidData(){
        return $this->hasOne(BidData::className(), ['bid_id' => 'bid_id']);
    }
    
    
    public function getUser2suplier(){
        return $this->hasOne(User2suplier::className(), ['user_id' => 'user_id']);
    }
}
