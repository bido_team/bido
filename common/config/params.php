<?php
Yii::setAlias('@phonesImgPath', 'C:\xampp\htdocs\BIDO\common\web\img\phones');
Yii::setAlias('@phonesImgUrl', 'http://localhost/BIDO/common/web/img/phones');
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration' => 3600,
];
