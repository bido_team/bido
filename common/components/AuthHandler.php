<?php
namespace common\components;

use common\models\Auth;
use common\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;
    private $email;
    private $id;
    private $nickname;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();
        $this->setAuthAttr($attributes);
        
        /* @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $this->id,
        ])->one();
        
        if (Yii::$app->user->isGuest) {
            $this->processGuest($auth);
        } else { // user already logged in
            $this->processUser($auth, $attributes);
        }
    }
    
    private function setAuthAttr($attributes){
        $this->id = ArrayHelper::getValue($attributes, 'id');
        switch ($this->client->name){
            case 'facebook':
                $this->email = ArrayHelper::getValue($attributes, 'email');
                $this->nickname = ArrayHelper::getValue($attributes, 'name');
                break;
            case 'google':
                $this->email = ArrayHelper::getValue($attributes, 'emails.0.value');
                $this->nickname = ArrayHelper::getValue($attributes, 'displayName');
                break;
            default:
                $this->email = ArrayHelper::getValue($attributes, 'email');
                $this->nickname = ArrayHelper::getValue($attributes, 'name');
                break;
        }
    }
    
    private function processGuest($auth){
        if ($auth) { // login
            /* @var User $user */
            $user = $auth->user;
            Yii::$app->user->setReturnUrl(Yii::$app->session['referrer_url']);
            Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
        } else { // signup
            if ($this->email !== null && User::find()->where(['email' => $this->email])->exists()) {
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $this->client->getTitle()]),
                ]);
            } else {
                $this->signInViaAuth();
            }
        }
    }
    
    private function processUser($auth, $attributes) {
        if (!$auth) { // add auth provider
            $auth = new Auth([
                'user_id' => Yii::$app->user->id,
                'source' => $this->client->getId(),
                'source_id' => (string)$attributes['id'],
            ]);
            if ($auth->save()) {
                /** @var User $user */
                $user = $auth->user;
                Yii::$app->getSession()->setFlash('success', [
                    Yii::t('app', 'Linked {client} account.', [
                        'client' => $this->client->getTitle()
                    ]),
                ]);
            } else {
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app', 'Unable to link {client} account: {errors}', [
                        'client' => $this->client->getTitle(),
                        'errors' => json_encode($auth->getErrors()),
                    ]),
                ]);
            }
        } else { // there's existing auth
            Yii::$app->getSession()->setFlash('error', [
                Yii::t('app',
                    'Unable to link {client} account. There is another user using it.',
                    ['client' => $this->client->getTitle()]),
            ]);
        }
    }
    
    
    private function signInViaAuth() {
        $password = Yii::$app->security->generateRandomString(6);
        $user = new User([
            'username' => $this->nickname,
            'email' => $this->email,
            'password' => $password,
        ]);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();

        $transaction = User::getDb()->beginTransaction();

        if ($user->save()) {
            $auth = new Auth([
                'user_id' => $user->id,
                'source' => $this->client->getId(),
                'source_id' => (string)$this->id,
            ]);
            if ($auth->save()) {
                $transaction->commit();
                Yii::$app->user->setReturnUrl(Yii::$app->session['referrer_url']);
                Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
            } else {
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app', 'Unable to save {client} account: {errors}', [
                        'client' => $this->client->getTitle(),
                        'errors' => json_encode($auth->getErrors()),
                    ]),
                ]);
            }
        } else {
            Yii::$app->getSession()->setFlash('error', [
                Yii::t('app', 'Unable to save user: {errors}', [
                    'client' => $this->client->getTitle(),
                    'errors' => json_encode($user->getErrors()),
                ]),
            ]);
        }
    }
}