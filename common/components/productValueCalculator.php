<?php

namespace common\components;

use Yii;
use yii\base\Component;
use common\models\UniqueProducts;
use common\models\MainProducts;

class productValueCalculator extends Component {
    
    private $value_points = [];
    private $product_score = [];
    
    private $unique_product = null;
    private $product_category_id = null;
    
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
    }
    
    public  function calculate($calculation_type, $unique_product_id){
        $this->unsetData();
        $up_model = UniqueProducts::findOne($unique_product_id);
        $this->__set('unique_product', $unique_product_id);
        $this->__set('product_category_id', $up_model->category_id);
        $data_for_calculation = self::getDataForCalculation($up_model);
        if(!empty($data_for_calculation)){
            switch($calculation_type){
                case 1 :
                    $most_valued_product_id =  self::calculateOverAllValue($data_for_calculation);
                    print_r($this->unique_product);
                    print_r($data_for_calculation);
                    break;
    //            TODO: add more calculatio types;
            }
            if(isset($most_valued_product_id)){
                self::setMainProduct($most_valued_product_id);
            }
        }
    }
    
    private function getDataForCalculation($up_model){
        $value_point = 1;
        foreach($up_model->products as $product){
            $value_points[] = $value_point;
            $value_point++;
            
            $data_for_calculation['price'][$product->id] = $product->price;
            $data_for_calculation['delivery_time'][$product->id] = $product->delivery_time;
            $data_for_calculation['guaranty'][$product->id] = $product->guaranty;
            $data_for_calculation['rating'][$product->id] = $product->suplier->rating;
            $data_for_calculation['creation_time'][$product->id] = $product->time;
            $data_for_calculation['amount'][$product->id] = $product->amount;
            $data_for_calculation['suplier_id'][$product->id] = $product->suplier->id;
        }
        self::__set('value_points',$value_points);
        return $data_for_calculation ?? [];
    }
    
    private function calculateOverAllValue($data_for_calculation){
        arsort($data_for_calculation['price']);
        self::setValuePointsForSingleComponent($data_for_calculation['price']);
        arsort($data_for_calculation['delivery_time']);
        self::setValuePointsForSingleComponent($data_for_calculation['delivery_time']);
        asort($data_for_calculation['guaranty']);
        self::setValuePointsForSingleComponent($data_for_calculation['guaranty']);
        asort($data_for_calculation['rating']);
        self::setValuePointsForSingleComponent($data_for_calculation['rating']);
        return self::getProductWithBestValue($data_for_calculation);
    }
    
    private function setValuePointsForSingleComponent($sorted_values){
        $array_position = 0;
        foreach($sorted_values as $product_id => $component_value){
            if(isset($previous_component_value) && $component_value !== $previous_component_value){
                $array_position++;
            }
            $product_value_points = $this->product_score[$product_id] ?? 0;
            $this->product_score[$product_id] = $product_value_points + $this->value_points[$array_position];
            $previous_component_value = $component_value;
        }
    }
    
    private function getProductWithBestValue($data_for_calculation){
        $calculation_type = 1;
        while(!self::chechIfBestProductFound()){
            switch ($calculation_type){
                case 1 : 
                    arsort($data_for_calculation['creation_time']);
                    self::setValuePointsForSingleComponent($data_for_calculation['creation_time']);
                    break;
                case 2 :
                    asort($data_for_calculation['amount']);
                    self::setValuePointsForSingleComponent($data_for_calculation['amount']);
                    break;
                case 3 : 
                    arsort($data_for_calculation['suplier_id']);
                    self::setValuePointsForSingleComponent($data_for_calculation['suplier_id']);
                    break;
            }
            $calculation_type++;
        }
        $best_product_id = array_keys($this->product_score ,max ($this->product_score));
        return $best_product_id[0];
    }
    
    private function chechIfBestProductFound(){
        $best_products = array_keys($this->product_score ,max ($this->product_score));
        return (count($best_products) > 1) ? false : true;
    }
    
    private function setMainProduct($most_valued_product_id){
        $main_prodcuts = MainProducts::find()->where(['unique_product_id' => $this->unique_product])->one();
        if(empty($main_prodcuts)){
            return $this->saveMainProduct($most_valued_product_id);
        }
        return $this->updateMainProduct($main_prodcuts);
    }
    
    private function saveMainProduct($most_valued_product_id){
        $main_prodcuts_model = new MainProducts();
        $main_prodcuts_model->unique_product_id = $this->unique_product;
        $main_prodcuts_model->product_id = $most_valued_product_id;
        $main_prodcuts_model->category_id = $this->product_category_id;
        if($main_prodcuts_model->validate()){
            return $main_prodcuts_model->save();
        }
        return false;
    }
    
    private function updateMainProduct($main_prodcuts){
        $main_prodcuts->product_id = $most_valued_product_id;
        if($main_prodcuts_model->validate()){
            return $main_prodcuts_model->save();
        }
        return false;
    }
    
    private function unsetData() {
        $this->__set('value_points', []);
        $this->__set('product_score', []);
        $this->__set('unique_product', null);
        $this->__set('product_category_id', null);
    }
}


