<?php

use yii\db\Migration;

/**
 * Class m181202_175919_products_table
 */
class m181202_175919_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
                
        $this->execute("CREATE TABLE `supliers` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(255) CHARACTER SET utf8 NOT NULL,
                `rating` float,
                `description` text CHARACTER SET utf8,
                `img` text CHARACTER SET utf8,
                `email` varchar(255) CHARACTER SET utf8,
                `phone_number` int(11),
                `adress` text CHARACTER SET utf8,
                PRIMARY KEY(id)
            );"
        );
                
        $this->execute("CREATE TABLE `products` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `suplier_id` int(11) NOT NULL,
                `price` float NOT NULL,
                `amount` int(11) NOT NULL,
                `delivery_time` int(11) NOT NULL,
                `guaranty` int(11) NOT NULL,
                `unique_product_id` int(11) NOT NULL,
                `time` DATETIME(3) NOT NULL,
                PRIMARY KEY(id),
                FOREIGN KEY (suplier_id) REFERENCES supliers(id),
                FOREIGN KEY (unique_product_id) REFERENCES unique_products(id)
            );"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181202_175919_products_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181202_175919_products_table cannot be reverted.\n";

        return false;
    }
    */
}
