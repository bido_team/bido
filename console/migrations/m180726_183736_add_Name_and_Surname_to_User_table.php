<?php

use yii\db\Migration;

/**
 * Class m180726_183736_add_Name_and_Surname_to_User_table
 */
class m180726_183736_add_Name_and_Surname_to_User_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'name', 'varchar(32)');
        $this->addColumn('user', 'surname', 'varchar(32)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180726_183736_add_Name_and_Surname_to_User_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180726_183736_add_Name_and_Surname_to_User_table cannot be reverted.\n";

        return false;
    }
    */
}
