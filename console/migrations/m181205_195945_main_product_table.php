<?php

use yii\db\Migration;

/**
 * Class m181205_195945_main_product_table
 */
class m181205_195945_main_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `main_products` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `product_id` int(11) NOT NULL,
                `unique_product_id` int(11) NOT NULL,
                `category_id` int(11) NOT NULL,
                `time` datetime(3) NOT NULL,
                PRIMARY KEY(id),
                FOREIGN KEY (product_id) REFERENCES products(id),
                FOREIGN KEY (unique_product_id) REFERENCES unique_products(id),
                FOREIGN KEY (category_id) REFERENCES product_categories(id)
            );"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181205_195945_main_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181205_195945_main_product_table cannot be reverted.\n";

        return false;
    }
    */
}
