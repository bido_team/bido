<?php

use yii\db\Migration;

/**
 * Class m181202_182315_confirmed_orders_table
 */
class m181202_182315_confirmed_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
        
        $this->execute("CREATE TABLE `confirmed_orders` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `product_id` int(11) NOT NULL,
                `unique_product_id` int(11) NOT NULL,
                `order_amount` int(11) NOT NULL,
                `user_id` int(11) NOT NULL,
                `time` datetime(3) NOT NULL,
                PRIMARY KEY(id),
                FOREIGN KEY (product_id) REFERENCES products(id),
                FOREIGN KEY (user_id) REFERENCES user(id).
                FOREIGN KEY (unique_product_id) REFERENCES unique_products(id)
            );"
        );
        
        $this->execute("CREATE TABLE `confirmed_order_data` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `confirmed_order_id` int(11) NOT NULL,
                `price` int(11) NOT NULL,
                `delivery_time` int(11) NOT NULL,
                `guaranty` int(11) NOT NULL,
                PRIMARY KEY(id),
                FOREIGN KEY (confirmed_order_id) REFERENCES confirmed_orders(id)
            );"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181202_182315_confirmed_orders_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181202_182315_confirmed_orders_table cannot be reverted.\n";

        return false;
    }
    */
}
