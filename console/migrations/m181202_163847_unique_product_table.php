<?php

use yii\db\Migration;

/**
 * Class m181202_163847_unique_product_table
 */
class m181202_163847_unique_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `product_categories` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(255) CHARACTER SET utf8 NOT NULL,
                `description` text CHARACTER SET utf8 NOT NULL,
                `img` text CHARACTER SET utf8 NOT NULL,
                PRIMARY KEY(id)
            );"
        );
        
        $this->execute("CREATE TABLE `language_codes` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `code` varchar(5) CHARACTER SET utf8 NOT NULL,
                `title` varchar(255) CHARACTER SET utf8 NOT NULL,
                PRIMARY KEY(id)
            );"
        );
        
        $this->execute("INSERT INTO `language_codes`(`id`, `code`, `title`)
            VALUES (1, 'en-US', 'English'),(2, 'ru-RU', 'Pусский'), (3, 'lt-LT', 'Lietuvių');"
        );
        
        $this->execute("CREATE TABLE `product_categories_trans` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `category_id` int(11) NOT NULL,
                `lang` int(11) NOT NULL,
                `title` varchar(255) CHARACTER SET utf8 NOT NULL,
                `description` text CHARACTER SET utf8 NOT NULL,
                PRIMARY KEY(id),
                FOREIGN KEY (category_id) REFERENCES product_categories(id),
                FOREIGN KEY (lang) REFERENCES language_codes(id)
            );"
        );
        
        $this->execute("CREATE TABLE `unique_products` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `title` varchar(255) CHARACTER SET utf8 NOT NULL,
                `description` text CHARACTER SET utf8 NOT NULL,
                `img` text CHARACTER SET utf8 NOT NULL,
                `category_id` int(11) NOT NULL,
                PRIMARY KEY(id),
                FOREIGN KEY (category_id) REFERENCES product_categories(id)
            );"
        );
        
        $this->execute("CREATE TABLE `unique_products_trans` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `unique_product_id` int(11) NOT NULL,
                `lang` int(11) NOT NULL,
                `title` varchar(255) CHARACTER SET utf8 NOT NULL,
                `description` text CHARACTER SET utf8 NOT NULL,
                PRIMARY KEY(id),
                FOREIGN KEY (unique_product_id) REFERENCES unique_products(id),
                FOREIGN KEY (lang) REFERENCES language_codes(id)
            );"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181202_163847_unique_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181202_163847_unique_product_table cannot be reverted.\n";

        return false;
    }
    */
}
