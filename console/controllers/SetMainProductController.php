<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\UniqueProducts;

class SetMainProductController extends Controller {

    public function actionIndex() {
        $unique_products = UniqueProducts::find()->all();
        foreach($unique_products as $product) {
            Yii::$app->productValueCalculator->calculate(1, $product->id);
        }
    }
}