/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    $( "#best-bid-deal" ).click(function() {
        chosenBid($(this));
    });
});

function chosenBid(pressed_element){
    var bid_data = getBidData(pressed_element);
    postSelectedBid(bid_data);
};

function getBidData(pressed_element){
    return bid_data = {
        'BID_DATA' : {
            'PRICE' : pressed_element.find('.bid-price').text(),
            'DISCOUNT' : pressed_element.find('.bid-discount').text(),
            'DISCOUNT_PRICE' : pressed_element.find('.bid-discount-amount').text(),
            'BIDDER' : pressed_element.find('.bid-bidder').text(),
            'DELIVERY' : pressed_element.find('.bid-delivery').text(),
            'GUARANTY' : pressed_element.find('.bid-guaranty').text(),
            'RATING' : pressed_element.find('.bid-rating').text()
        }
    };
}

function postSelectedBid(bid_data){
    $.ajax({
        url: '/bids/selectbid',
        type: 'post',
        data: bid_data,
        success: function (data) {
           console.log(data.search);
        }
    });
}