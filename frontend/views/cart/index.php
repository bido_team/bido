<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\db\ActiveQuery;
use yii\web\Session;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$session = Yii::$app->session;
$this->title = Yii::t('front-end', 'Cart');
$this->registerJsFile(
    '@web/js/cart.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?php
    $query = new Query();
    $provider = new ArrayDataProvider([
        'allModels' => $products,
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['style' => 'width:40px'],
            ],
            [
                'label' => Yii::t('front-end', 'Title'),
                'value' => function($model) {
                    return $model['title'];
                },
            ],
            [
                'label' => Yii::t('front-end', 'Amount'),
                'value' => function($model) {
                    return $model['amount'];
                },
                'headerOptions' => ['style' => 'width:80px'],
            ],
            [ 
                'label' => Yii::t('front-end', 'Item Price'),
                'value' => function($model) {
                    return $model['price'];
                },
                'headerOptions' => ['style' => 'width:120px'],
            ],
            [
                'label' => Yii::t('front-end', 'Total Price'),
                'value' => function($model) {
                    return $model['price'] * $model['amount'];
                },
                'headerOptions' => ['style' => 'width:80px'],
            ],
            [
                'format' => 'html',
                'value' => function($model) {
                    return Html::img(Yii::getAlias('@phonesImgUrl').'/'.$model['img'],
                        ['width' => '100px']);
                },
                'headerOptions' => ['style' => 'width:80px'],
            ],
            [  
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:260px;'],
                'header'=>'Actions',
                'template' => '{Plus} {Minus} {Confirm} {View}',
                'buttons' => [
                    
                    //confirm button
                    'Confirm' => function ($url, $model) {
                        if(empty($model['confirmed'])) {
                            return Html::a(Yii::t('front-end', 'Confirm'),
                                false, 
                                [
                                    'value' => (Yii::$app->user->isGuest) 
                                        ? Url::to(['/users/login', 'confirm_action' => true]) 
                                        : Url::to(['/cart/payment']),
                                    'class'=>'btn btn-primary cart_action_btn confirm_order_button',
                                ]
                            );
                        }
                        return '';
                    },
                            
                    //plus button
                    'Plus' => function ($url, $model) {
                        if(empty($model['confirmed'])) {
                            return Html::a('+',
                                ['/cart/add', 'id' => $model['id']], 
                                [
                                    'class'=>'btn btn-success cart_action_btn'
                                ]
                            );
                        }
                        return '';
                    },
                    //minus button
                    'Minus' => function ($url, $model) {
                        if(empty($model['confirmed'])) {
                            return Html::a('-',
                                ['/cart/minus', 'id' => $model['id']], 
                                [
                                    'class'=>'btn btn-danger cart_action_btn'
                                ]
                            );
                        }
                        return '';
                    },
                            
                    //view button
                    'View' => function ($url, $model) {
                        if(!empty($model['confirmed'])) {
                            return Html::a(Yii::t('front-end', 'View'),
                                ['/cart/view', 'id'=>$model['id']],
                                [
                                    'class'=>'btn btn-primary cart_action_btn', 
                                ]
                            );
                        }
                        return '';
                    },
                ],
            ]
        ],
        'showOnEmpty' => false,
    ]); ?>
    <?php Modal::begin([
            'id' => 'confirm_modal',
            'size' => 'modal-md'
        ]);
        
        echo '<div id="confirm_modal_content" class="clearfix text-center"></div>';

        Modal::end();
    ?>
</div>
