<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
if($confirm_action){
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
    }
}

?>

<div class="site-login col-lg-6 col-lg-offset-3 container">
    
    <?php
    //check if login is initiated via confirm product button:
    
    ?>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'options' => ['id' => 'login-form']
    ]); ?>
    
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => 
            "<div class=\"\">{input} {label}</div>\n<div class=\"\">{error}</div>",
        ]) ?>
        
        <div class="form-group row">
            <div class="">
                <?= Html::submitButton('Login', ['class' => 'btn btn-success col-lg-4 col-lg-offset-1 sign-in-modal', 'name' => 'login-button']) ?>
                <?= Html::button('Sign-up', ['value' => Url::to('/users/signup'),'class' => 'btn btn-primary col-lg-4  col-lg-offset-2 sign-up-modal', 'name' => 'signup-button']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    
    <div class="row">
        <label for="auth_clients" class="col-lg-6 col-lg-offset-3">Login via:</label>
    </div>
    <div class="row">
        <?= yii\authclient\widgets\AuthChoice::widget([
            'baseAuthUrl' => ['users/auth'],
            'popupMode' => false,
            'options' => [
                'class' => 'col-lg-7 col-lg-offset-2'
            ]
       ]) ?>
   </div>
</div>

<?php $ajax_signup_script = <<< JS
    $('.sign-up-modal').click(function (){
        $('#confirm_modal').modal('show')
                .find('#confirm_modal_content')
                .load($(this).attr('value'));
    });
JS;
$this->registerJs($ajax_signup_script);  
?>
