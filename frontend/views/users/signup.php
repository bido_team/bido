<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sign up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup col-lg-6 col-lg-offset-3 container">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="">
            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'enableAjaxValidation' => true]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
            
                <?= $form->field($model, 'repeat_password')->passwordInput() ?>

                <div class="form-group row">
                    <div class="">
                        <?= Html::submitButton('Sign-up', ['class' => 'btn btn-success col-lg-4 col-lg-offset-1', 'name' => 'signup-button']) ?>
                        <p class="text-center col-lg-2"> <?= Yii::t('app', 'OR') ?></p>
                        <?= Html::button('Login', ['value' => Url::to('/users/login'),'class' => 'btn btn-primary col-lg-4 sign-in-modal']) ?>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php $ajax_signin_script = <<< JS
    $('.sign-in-modal').click(function (){
        $('#confirm_modal').modal('show')
                .find('#confirm_modal_content')
                .load($(this).attr('value'));
    });
JS;
$this->registerJs($ajax_signin_script);  
?>
