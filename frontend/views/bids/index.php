<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\Session;
use frontend\assets\BidAsset;


/* @var $this yii\web\View */
/* @var $searchModel app\models\TIRESSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
BidAsset::register($this);
$session = Yii::$app->session;
$this->title = Yii::t('app', 'Tires');
?>
<div class="tires-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php  
    
    
    ?>
    <div class="col-md-6">

<!--Row For Image and Short Description-->

<div class="row">

    <div class="col-md-7">
       <img class="img-responsive" src="<?php echo $model["productInfo"]->img ?>" alt="">

    </div>

    <div class="col-md-5">

        <div class="thumbnail">
         

    <div class="caption-full">
        <h4><a href="#"><?php echo $model["products"]->title ?></a> </h4>
        <hr>
        <h4 class=""><?php echo  Yii::$app->formatter->asCurrency($model->order_price, '€') ?></h4>

    <div class="ratings">
     
        <p>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            4.0 stars
        </p>
    </div>

    </div>
 
</div>

</div>


</div><!--Row For Image and Short Description-->


        <hr>


<!--Row for Tab Panel-->

<div class="row">

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">

<p></p>
           
    <p><?php echo $model["productInfo"]->description ?></p>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">

  <div class="col-md-6">

       <h3>2 Reviews From </h3>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star-empty"></span>
                Anonymous
                <span class="pull-right">10 days ago</span>
                <p>This product was great in terms of quality. I would definitely buy another!</p>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star-empty"></span>
                Anonymous
                <span class="pull-right">12 days ago</span>
                <p>I've alredy ordered another one!</p>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star"></span>
                <span class="glyphicon glyphicon-star-empty"></span>
                Anonymous
                <span class="pull-right">15 days ago</span>
                <p>I've seen some better than this, but not at this price. I definitely recommend this item.</p>
            </div>
        </div>

    </div>


    <div class="col-md-6">
        <h3>Add A review</h3>

     <form action="" class="form-inline">
        <div class="form-group">
            <label for="">Name</label>
                <input type="text" class="form-control" >
            </div>
             <div class="form-group">
            <label for="">Email</label>
                <input type="test" class="form-control">
            </div>

        <div>
            <h3>Your Rating</h3>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
        </div>

            <br>
            
             <div class="form-group">
             <textarea name="" id="" cols="60" rows="10" class="form-control"></textarea>
            </div>

             <br>
              <br>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="SUBMIT">
            </div>
        </form>

    </div>

 </div>

 </div>

</div>


</div><!--Row for Tab Panel-->




</div><!-- col-md-9 end here -->

<div class="col-md-6" id="bids-sidebar">
    <div id="best-bid-deal" class="row">
        <div class="bid-button-discount">
            <h2>
                <span class="bid-discount">-25</span>%
            </h2>
            <p>
                <strong><?php echo Yii::t("front_end", "Saving") . ": " ?></strong>
                <span class="bid-discount-amount">30</span>eur
            </p>
        </div>
        <div class="bid-button-price">
            <h4 class="">
                <strong><?php echo Yii::t("front_end", "Price") . ": " ?></strong>
                <span class="bid-price">500</span>
                <span style="color: #8EBF5F">(-25%)</span>
            </h4>   
            <h4 class="">
                <strong><?php echo Yii::t("front_end", "Bidder") . ": " ?></strong>
                <span class="bid-bidder">TELE2</span>
            </h4> 
        </div>
        <div class="bid-button-data">
            <p class="">
                <strong><?php echo Yii::t("front_end", "Delivery") . ": " ?></strong>
                <span class="bid-delivery">1 day</span>
                <span style="color: #8EBF5F">(-25%)</span> 
            </p>
            <p class="">
                <strong><?php echo Yii::t("front_end", "Guaranty") . ": " ?></strong>
                <span class="bid-guaranty">500</span>
                <span style="color: #8EBF5F">(-25%)</span> 
            </p> 
            <p class="">
                <strong><?php echo Yii::t("front_end", "Rating") . ": " ?></strong>
                <span class="bid-rating">500</span>
                <span style="color: #8EBF5F">(-25%)</span> 
            </p> 
        </div>
    </div>
    
    <div id="bid-options">
        <?php // if(isset($aa)){
            //foreach($aa as $a){ ?>
        
            <div class="row bid-deal">
                    <div class="bid-button-discount">
                        <h2 class="bid-price">-20%</h2>
                        <p class=""><strong><?php echo Yii::t("front_end", "Saving") . ": " ?></strong>25eur</p>
                    </div>
                    <div class="bid-button-price">
                        <h4 class="">
                            <strong><?php echo Yii::t("front_end", "Price") . ": " ?></strong>
                            <span> 450 </span>
                            <span style="color: #8EBF5F">(-20%)</span> 
                        </h4>   
                        <h4 class="">
                            <strong><?php echo Yii::t("front_end", "Suplier") . ": " ?></strong>
                            <span>TELE2</span>
                        </h4> 
                    </div>
                    <div class="bid-button-data">
                        <p class="">
                            <strong><?php echo Yii::t("front_end", "Delivery") . ": " ?></strong>
                            <span> 450 </span>
                            <span style="color: #8EBF5F">(-20%)</span> 
                        </p>
                        <p class="">
                            <strong><?php echo Yii::t("front_end", "Guaranty") . ": " ?></strong>
                            450 
                            <span style="color: #8EBF5F">(-20%)</span> 
                        </p> 
                        <p class="">
                            <strong><?php echo Yii::t("front_end", "Rating") . ": " ?></strong>
                            <span>450</span>
                            <span style="color: #8EBF5F">(-20%)</span> 
                        </p> 
                    </div>

            </div>
        
        <?php //}
        //} else {
        //    echo "there are no bids";
        //}
?>
    </div>
</div>
</div>