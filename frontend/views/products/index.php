<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register($this);
$this->title = Yii::t('app', 'Tires');
?>
<div class="tires-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php  
    
    foreach($products as $product){
        echo Html::tag('div', 
            Html::tag('div',
                    Html::a( 
                        Html::img( $product["productInfo"]->img, $options = ['class'=>'products_img']),
                        ['item', 'id' => $product->id],
                        ['class'=>'']
                    )
                    . Html::tag('div',
                        Html::tag('h4', Yii::$app->formatter->asCurrency($product->price, '€'),['class' => 'pull-right'])
                        . Html::tag('h4',
                            Html::a( $product->title,
                                [Yii::$app->request->baseUrl.'/controllers/products/item?id='.$product->id],
                                ['class'=>'']
                            ),   
                            ['class' => '']
                        )
                        . Html::tag('p', $product["productInfo"]->description,['class'=>''])
                        . Html::a( Yii::t('front-end', 'Add to cart'), ['/cart/add', 'id' => $product->id],   ['class'=>'btn btn-primary']),
                        ['class'=>'caption']
                    ),
                ['class'=>'thumbnail']
            ),
            ['class'=>'col-sm-4 col-lg-4 col-md-4 products']
        );
    }    
    ?>
    
</div>
