<?php
use yii\bootstrap\Html;
use yii\web\Session;
use yii\helpers\Url;

/* @var $this yii\web\View */
$session = Yii::$app->session;
Yii::$app->language = "en-US";
$this->title = 'Bido.lt';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo Html::img('/img/Bido_logo_black.png', ['class' => 'bido_logo_main_page']) ?></h1>

        <p class="lead"><?php echo Yii::t('front-end', 'New way of shopping!'); ?></p>

        <p><?= Html::a( Yii::t('front-end', 'Find out more!'), ['/users/'], ['class'=>'btn btn-primary']) ?></p>
    </div>

    <div class="body-content">

        <div class="row">
            <?php foreach($main_products as $main_product){ ?>
            
            <?= Html::tag('div', 
                    Html::tag('div',
                            Html::a( 
                                Html::img(
                                    Yii::getAlias('@phonesImgUrl').'/'.$main_product->uniqueProduct->img,
                                    $options = ['class'=>'products_img']
                                ),
                                ['item', 'id' => $main_product->id],
                                ['class'=>'']
                            )
                            . Html::tag('div',
                                Html::tag('h4', Yii::$app->formatter->asCurrency($main_product->product->price, '€'),['class' => 'pull-right'])
                                . Html::tag('h4',
                                    Html::a( $main_product->uniqueProduct->title,
                                        ['item', 'id' => $main_product->id],
                                        ['class'=>'']
                                    ),   
                                    ['class' => '']
                                )
                                . Html::tag('p', $main_product->uniqueProduct->description,['class'=>''])
                                . Html::a( Yii::t('front-end', 'Add to cart'), ['/cart/add', 'id' => $main_product->product_id],   ['class'=>'btn btn-primary']),
                                ['class'=>'caption']
                            ),
                        ['class'=>'thumbnail']
                    ),
                    ['class'=>'col-sm-4 col-lg-4 col-md-4 products']
                );
            ?>
            <?php }?>
        </div>

    </div>
</div>
