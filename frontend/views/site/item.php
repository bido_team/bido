<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\Session;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TIRESSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$session = Yii::$app->session;
$this->title = $product_model->category->title
?>
<div class="tires-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-9">

<!--Row For Image and Short Description-->

<div class="row">

    <div class="col-md-7">
       <img class="img-responsive" src="<?= Yii::getAlias('@phonesImgUrl').'/'.$product_model->uniqueProduct->img ?>" alt="">
    </div>
    <div class="col-md-5">

        <div class="thumbnail">
         

    <div class="caption-full">
        <h4><a href="#"><?php echo $product_model->uniqueProduct->title ?></a> </h4>
        <hr>
        <h4 class=""><?php echo  Yii::$app->formatter->asCurrency($product_model->product->price, '€') ?></h4>

<!--    <div class="ratings">
        <p>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            4.0 stars
        </p>
    </div>-->
   
        <form action="">
            <div class="form-group">
                <?php echo Html::a(Yii::t('front-end', 'Add'), ['/cart/add', 'id' => $product_model->product_id] , ['class'=>'btn btn-primary'])?>
            </div>
        </form>
    </div>
</div>

</div>


</div><!--Row For Image and Short Description-->


        <hr>


<!--Row for Tab Panel-->

<div class="row">

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
    <!--<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a></li>-->

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <p><?= $product_model->uniqueProduct->description ?></p>
    </div>
<!--    <div role="tabpanel" class="tab-pane" id="profile">
        <div class="col-md-6">
             <h3>2 Reviews From </h3>

              <hr>

              <div class="row">
                  <div class="col-md-12">
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star-empty"></span>
                      Anonymous
                      <span class="pull-right">10 days ago</span>
                      <p>This product was great in terms of quality. I would definitely buy another!</p>
                  </div>
              </div>

              <hr>

              <div class="row">
                  <div class="col-md-12">
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star-empty"></span>
                      Anonymous
                      <span class="pull-right">12 days ago</span>
                      <p>I've alredy ordered another one!</p>
                  </div>
              </div>

              <hr>

              <div class="row">
                  <div class="col-md-12">
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star"></span>
                      <span class="glyphicon glyphicon-star-empty"></span>
                      Anonymous
                      <span class="pull-right">15 days ago</span>
                      <p>I've seen some better than this, but not at this price. I definitely recommend this item.</p>
                  </div>
              </div>

          </div>


    <div class="col-md-6">
        <h3>Add A review</h3>

     <form action="" class="form-inline">
        <div class="form-group">
            <label for="">Name</label>
                <input type="text" class="form-control" >
            </div>
             <div class="form-group">
            <label for="">Email</label>
                <input type="test" class="form-control">
            </div>

        <div>
            <h3>Your Rating</h3>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
        </div>

            <br>
            
             <div class="form-group">
             <textarea name="" id="" cols="60" rows="10" class="form-control"></textarea>
            </div>

             <br>
              <br>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="SUBMIT">
            </div>
        </form>

    </div>

 </div>-->

 </div>

</div>


</div><!--Row for Tab Panel-->




</div><!-- col-md-9 end here -->
</div>
