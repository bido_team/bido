<?php

/* @var $this \yii\web\View */
/* @var $content string */
use common\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use kartik\icons\Icon;
use common\models\ConfirmedOrders;
use yii\web\Session;

$session = Yii::$app->session;
if (!($session->isActive))
{
    $session->open();
    $session['total_items'] = 0;
}
AppAsset::register($this);
Icon::map($this);  
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/img/favicon2.png" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php 
    $items_in_cart = $session['total_items'];
    if(!Yii::$app->user->isGuest){
        $items_in_cart += ConfirmedOrders::countUserComfirmedOrders();
    }
?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('/img/Bido_logo.png', ['class' => 'bido_logo']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('front-end', 'Home'), 'url' => ['/site/index']],
            ['label' => Yii::t('front-end', 'Cart ') . $items_in_cart, 'url' => ['/cart/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => Yii::t('front-end', 'Contact'), 'url' => ['/site/contact']],
            ['label' => Yii::t('front-end', 'Destroy session'), 'url' => ['/site/close_session']],
            Yii::$app->user->isGuest ? (
                ['label' => Yii::t('front-end', 'Login'), 'url' => ['/users/login']]
            ) : (
                '<li>'
            . Html::beginForm(['/users/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left col-md-9">&copy; Bido.com <?= date('Y') ?></p>
        <div class="col-md-3 ">
            <?php echo Yii::t('front-end', 'Fallow us:')  ?>
            <a href="https://www.facebook.com/" target="_blank">
                <?php echo Icon::show('fb', ['class'=>'fa-facebook-square'], Icon::FA);  ?> 
            </a>
            <a href="https://www.youtube.com/" target="_blank">
                <?php echo Icon::show('youtube', ['class'=>'fa-youtube-play'], Icon::FA);  ?>
            </a>
            <a href="https://www.instagram.com/" target="_blank">
                <?php echo Icon::show('insta', ['class'=>'fa-instagram'], Icon::FA);  ?>
            </a>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
