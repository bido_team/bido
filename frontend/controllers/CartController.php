<?php

namespace frontend\controllers;

use Yii;
use common\models\Products;
use common\models\ProductsSearch;
use common\models\ConfirmedOrders;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use yii\web\Session;

/**
 * CartController implements the CRUD actions for Products model.
 */
class CartController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex() {
        $session = Yii::$app->session;
        $products = [];
        if($session['total_items'] > 0){
            $products = self::getProductsInCart();
        }
        return $this->render('index', [
            'products' => $products,
        ]);
    }
    
    /**
     * Get all products in users cart (Confirmed or in Session).
     * @return array $products_in_cart products in cart data.
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function getProductsInCart(){
        $products_in_cart = self::getSessionOrders();
        
        if(!Yii::$app->user->isGuest){ 
            $confirmed_orders = ConfirmedOrders::getUserComfirmedOrders();
            foreach($confirmed_orders as $order) {
                $products_in_cart[] = $this->getOrderedProductData($order, 'confirmed');
            }
        }
        
        return $products_in_cart;
    }
    
    /**
     * Get products added to cart during users Session (but still not confirmed).
     * @return array $cart_products not confirmed products in cart data.
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function getSessionOrders(){
        $order_ids = self::getProductdIdsInSession();
        $sessios_products = Products::getProductsBySpecificIds($order_ids);
        foreach ($sessios_products as $product){
            $cart_products[] =  self::getOrderedProductData($product);
        }
        return $cart_products;
    }
    
    /**
     * Takes all orders added to Session, extracts product ids and returns them.
     * @return array $order_ids ordered (not confirmed) product ids.
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function getProductdIdsInSession(){
        $session = Yii::$app->session;
        $order_ids = [];
        foreach($session as $session_name => $value){
            $session_name_data = explode("_", $session_name);
            $name = $session_name_data[0];
            if($name === 'products'){
                $order_ids[] = $session_name_data[1];
            }
        }
        return $order_ids;
    }
    
    private function getOrderedProductData($order, $order_type = 'not_confirmed'){
        switch ($order_type){
            case 'not_confirmed':
                $session = Yii::$app->session;
                $price = $order->price;
                $amount = $session['products_'.$order->id];
                $confirmed = 0;
                break;
            case 'confirmed':
                $amount = $order->order_amount;
                $price = $order->confirmedOrdersData->price;
                $confirmed = 1;
                break;
        }
        
        return [
            'id' => $order->id,
            'title' => $order->uniqueProduct->title,
            'amount' => $amount,
            'price' => $price,
            'img' => $order->uniqueProduct->img,
            'confirmed' => $confirmed,
        ];
    }


    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $numb)
    {
        return $this->redirect(['/bids/index', 'id' => $id , 'numb' => $numb]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    //--------- ADD TO CART (beginning)------------\\
    public function actionAdd($id)
    {
        $model = $this->findModel($id);
        //add product to cart if product exists;
        (empty($model->id))? : self::addToCart($model);
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    private function addToCart($model)
    {
        $session = Yii::$app->session;
        $product_order_amount = (!isset($session['products_'.$model->id])) ? 1 : $session['products_'.$model->id] + 1;
        $add_up_numb = ($model->amount >= $product_order_amount) ? 1 : 0; 
        $session['products_'.$model->id] += $add_up_numb;
        $session['total_items'] += $add_up_numb;
    }
    
    //--------- ADD TO CART (end)------------\\
    
    //--------- MINUS FROM CART (beginning)------------\\
    public function actionMinus($id)
    {
        $model = $this->findModel($id);
        //add product to cart if product exists;
        if(!empty($model->id)){
            self::minusFromCart($model);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    private function minusFromCart($model)
    {
        $session = Yii::$app->session;
        if(isset($session['products_'.$model->id])){
            $product_order_amount = $session['products_'.$model->id];
            $minus_down_numb = 1; 
            $session['products_'.$model->id] -= $minus_down_numb;
            $session['total_items'] -= $minus_down_numb;
            if($product_order_amount < 2){
                unset($session['products_'.$model->id]);
            }
        }
    }
    
    //--------- MINUS FROM CART (end)------------\\
    
    //--------- CONFIRM PRODUCT (beginning)------------\\
    public function actionConfirm($id, $numb){
        $session = Yii::$app->session;
        $pro_model = $this->findModel($id);
        $conf_order_model  = new ConfirmedOrders();
        
        $conf_order_model->attributes = [
            'product_id' => $id,
            'order_numb' => $numb,
            'order_amount' => $session['products_'.$id. "_" . $numb],
            'order_price' => $pro_model->price,
            'user_ip' => self::getRealIpAddr(),
        ];
        if($conf_order_model->save()){
            $session['confirmed_'.$id.'_'.$numb] = $conf_order_model->id;
            return $this->redirect(['/bids/index', 'id' => $id , 'numb' => $numb]);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    //--------- CONFIRM PRODUCT (end)------------\\
    
    
    private function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    
    public function actionPayment() 
    {
        if (Yii::$app->user->isGuest || !Yii::$app->request->isAjax) {
            return $this->goBack();
        }
        
        return $this->renderAjax('payment');
    }
}
