<?php 
use yii\helpers\Html;

$username = Yii::$app->user->identity->username;
$name = Yii::$app->user->identity->name;
$surname = Yii::$app->user->identity->surname;
$first_name = (!empty($name)) ? $name : $username;
$full_name = (!empty($name)) ? $name." ".$surname : $username;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?php echo $full_name ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php $menuItems = [
            ['label' => Yii::t('backend', 'Home'), 'url' => ['/site/index']],
            ['label' => Yii::t('backend', 'My Products'), 'url' => ['/products/index']],
            ['label' => Yii::t('backend', 'Started Bids'), 'url' => ['/bids/index']],
        ];
        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
            $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
        } else {
            $menuItems[] = ['label' => 'Logout (' . $first_name . ')', 'url' => ['/site/logout'], 'option' => ['method' => 'post']];
        } ?> 
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems,
            ]
        ) ?>

    </section>

</aside>
