<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Bids */

$this->title = Yii::t('app', 'Update Bids: {nameAttribute}', [
    'nameAttribute' => $model->bid_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bids'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bid_id, 'url' => ['view', 'id' => $model->bid_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bids-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
