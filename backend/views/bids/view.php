<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\Session;
use backend\assets\BidAsset;


/* @var $this yii\web\View */
/* @var $searchModel app\models\TIRESSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
BidAsset::register($this);
$session = Yii::$app->session;
$this->title = Yii::t('app', 'Offers');

?>
<div class="tires-index">
    <input type="hidden" id="confirmed_order_id" value=<?php echo $co_model->id ?> />
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php  
    
    
    ?>
    <div class="col-md-6">

<!--Row For Image and Short Description-->

<div class="row">
    
    <div class="col-md-7">
        <input type="hidden" id="selected_order_id" value=<?php echo $co_model->id ?> />
        <img class="img-responsive" src="<?php echo $co_model["productInfo"]->img ?>" alt="">

    </div>

    <div class="col-md-5">

        <div class="thumbnail">
         

    <div class="caption-full">
        <h4><a href="#"><?php echo $co_model["products"]->title ?></a> </h4>
        <hr>
        <input type="hidden" value="<?php echo $co_model->order_price ?>"  id="selected_price"/> 
        <h4 class=""><?php echo  Yii::$app->formatter->asCurrency($co_model->order_price, '€') ?></h4>
        <input type="hidden" value="<?php echo $co_model->order_amount ?>"  id="selected_amount"/> 
        <h4 class=""><?php echo Yii::t("front_end", "Order amount") . ": " . $co_model->order_amount ?></h4>
        <input type="hidden" value="<?php echo $co_model["products"]->delivery_time ?>"  id="selected_delivery"/> 
        <h4 class=""><?php echo Yii::t("front_end", "Delivery time") . ": " . $co_model["products"]->delivery_time ?></h4>
        <input type="hidden" value="<?php echo $co_model["products"]->guaranty ?>"  id="selected_guaranty"/> 
        <h4 class=""><?php echo Yii::t("front_end", "Guaranty") . ": " . $co_model["products"]->guaranty ?></h4>
        <input type="hidden" value="<?php echo $co_model["products"]["bidoSupliers"]->title ?>"  id="selected_suplier"/> 
        <h4 class=""><?php echo Yii::t("front_end", "Suplier") . ": " . $co_model["products"]["bidoSupliers"]->title ?></h4>
        <input type="hidden" value="<?php echo $co_model["products"]["bidoSupliers"]->rating ?>"  id="selected_rating"/> 
        <h4 class=""><?php echo Yii::t("front_end", "Suplier rating") . ": " . $co_model["products"]["bidoSupliers"]->rating ?></h4>

    </div>
 
</div>

</div>


</div><!--Row For Image and Short Description-->




</div><!-- col-md-9 end here -->

<div class="col-md-6" id="bids-sidebar">
    <div id="best-bid-deal" class="row">
        <div class="bid-button-discount">
            <h2>
                <span class="bid-discount">-25</span>%
            </h2>
            <p>
                <strong><?php echo Yii::t("front_end", "Saving") . ": " ?></strong>
                <span class="bid-discount-amount">30</span>eur
            </p>
        </div>
        <div class="bid-button-price">
            <h4 class="">
                <strong><?php echo Yii::t("front_end", "Price") . ": " ?></strong>
                <span class="bid-price">500</span>
                <span style="color: #8EBF5F">(-25%)</span>
            </h4>   
            <h4 class="">
                <strong><?php echo Yii::t("front_end", "Bidder") . ": " ?></strong>
                <span class="bid-bidder">TELE2</span>
            </h4> 
        </div>
        <div class="bid-button-data">
            <p class="">
                <strong><?php echo Yii::t("front_end", "Delivery") . ": " ?></strong>
                <span class="bid-delivery">1 day</span>
                <span style="color: #8EBF5F">(-25%)</span> 
            </p>
            <p class="">
                <strong><?php echo Yii::t("front_end", "Guaranty") . ": " ?></strong>
                <span class="bid-guaranty">500</span>
                <span style="color: #8EBF5F">(-25%)</span> 
            </p> 
            <p class="">
                <strong><?php echo Yii::t("front_end", "Rating") . ": " ?></strong>
                <span class="bid-rating">500</span>
                <span style="color: #8EBF5F">(-25%)</span> 
            </p> 
        </div>
    </div>
    
    <!--      ALL BIDS:      -->
    <div id="bid-options">
        <?php if(isset($co_model["bids"][0])){
            foreach($co_model["bids"] as $bid){ 
                $total_savings_percent = round(100 - ((($bid["bidData"]["price"] * $co_model->order_amount) / ($co_model->order_price * $co_model->order_amount)) * 100) , 2 );
                $total_savings_price = round(($co_model->order_price * $co_model->order_amount) - ($bid["bidData"]["price"] * $co_model->order_amount) , 2 );
                
                if($total_savings_price >= 0){
                    
                }
        ?>
                <div class="row bid-deal">
                    <input type="hidden" class="unique_bid" value= <?php echo $bid->bid_id ?> />
                    <div class="bid-button-discount">
                        <h2 class="bid-price"><?php echo $total_savings_percent ?>%</h2>
                        <p class="">
                            <strong><?php echo Yii::t("front_end", "Saving") . ": " ?></strong>
                            <?php echo $total_savings_price . " eur" ?>
                        </p>
                    </div>
                    <div class="bid-button-price">
                        <h4 class="">
                            <strong><?php echo Yii::t("front_end", "Price") . ": " ?></strong>
                            <span><?php echo $bid["bidData"]["price"] ?></span>
                            <span style="color: #8EBF5F">(-20%)</span> 
                        </h4>   
                        <h4 class="">
                            <strong><?php echo Yii::t("front_end", "Suplier") . ": " ?></strong>
                            <span><?php echo $bid["user2suplier"]["bidoSupliers"]["title"] ?></span>
                        </h4> 
                    </div>
                    <div class="bid-button-data">
                        <p class="">
                            <strong><?php echo Yii::t("front_end", "Delivery") . ": " ?></strong>
                            <span><?php echo $bid["bidData"]["delivery_time"] ?></span>
                            <span style="color: #8EBF5F">(-20%)</span> 
                        </p>
                        <p class="">
                            <strong><?php echo Yii::t("front_end", "Guaranty") . ": " ?></strong>
                            <?php echo $bid["bidData"]["guaranty"] ?> 
                            <span style="color: #8EBF5F">(-20%)</span> 
                        </p> 
                        <p class="">
                            <strong><?php echo Yii::t("front_end", "Rating") . ": " ?></strong>
                            <span><?php echo $bid["user2suplier"]["bidoSupliers"]["rating"] ?></span>
                            <span style="color: #8EBF5F">(-20%)</span> 
                        </p> 
                    </div>
                </div>
        <?php }
        } else {
            echo "there are no bids";
        }
        ?>
    </div>
</div>
</div>