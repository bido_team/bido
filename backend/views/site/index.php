<?php

/* @var $this yii\web\View */

$this->title = 'Bido.lt';

$username = Yii::$app->user->identity->username;
$name = Yii::$app->user->identity->name;
$surname = Yii::$app->user->identity->surname;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo (!empty($name))? Yii::t('backend', "Welcome")." ".$name."!" : Yii::t('backend', "Welcome")." ".$username."!" ; ?></h1>
    </div>

    <div class="body-content">
        <div class="container">
            
            <!--USER DATA-->
            <div class="row text_in_middle">
                <h2><?php echo  Yii::t('backend', "YOUR TOTAL"). ":" ?> </h2>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center"><?php echo Yii::t('backend', "STARTED BIDS") ?></div>
                        <div class="panel-body text-center seller-panel-count"><?php echo $user_started_bids ?></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center"><?php echo Yii::t('backend', "AVAILABLE BIDS") ?></div>
                        <div class="panel-body text-center seller-panel-count"><?php echo $available_bids ?></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center"><?php echo Yii::t('backend', "FINISHED BIDS") ?></div>
                        <div class="panel-body text-center seller-panel-count"> <?php echo $user_finished_bids ?></div>
                    </div>
                </div>
            </div>
            
            <!--USER SUPLIER DATA-->
            <div class="row text_in_middle">
                <h2><?php echo $suplier_data["title"] . " " .  Yii::t('backend', "TOTAL") . ":" ?> </h2>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center"><?php echo Yii::t('backend', "STARTED BIDS") ?></div>
                        <div class="panel-body text-center seller-panel-count"><?php echo $suplier_started_bids ?></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center"><?php echo Yii::t('backend', "AVAILABLE BIDS") ?></div>
                        <div class="panel-body text-center seller-panel-count"><?php echo $available_bids ?></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center"><?php echo Yii::t('backend', "FINISHED BIDS") ?></div>
                        <div class="panel-body text-center seller-panel-count"> <?php echo $suplier_finished_bids ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
