<?php

namespace backend\controllers;

use Yii;
use common\models\Bids;
use common\models\BidsSearch;
use common\models\ConfirmedOrders;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BidsController implements the CRUD actions for Bids model.
 */
class BidsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bids models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BidsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bids model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $co_model = ConfirmedOrders::findOne($model->confirmedOrders->id);
        return $this->render('view', [
            'model' => $model,
            'co_model' => $co_model
        ]);
    }

    /**
     * Creates a new Bids model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bids();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->bid_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Bids model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->bid_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Bids model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bids model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bids the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bids::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    public function actionBids($order_id){
        $confirmed_order_model = ConfirmedOrders::findOne($order_id);
        $bid_component = self::getBidComponent($confirmed_order_model);
        if(!empty($bids)){
            $response = [
                "success" => true,
                "bids" => $bids,
            ];
        } else {
            $response = [
                "success" => false,
            ];
        }
        echo json_encode($response);
    }
    
    private function getBidComponent($confirmed_order_model){
        $currently_selected_order = self::getCurrentlySelectedOrderData($confirmed_order_model);
        foreach($confirmed_order_model->bids as $bid){ 
            $bid_calculations = self::getCalculatedBidData($bid);
            $bid_compoennts[$bid->suplier_id] = self::bidComponent($bid_calculations, $bid);
        }
    }
    
    
    private function getCurrentSelectedOrderFromPost($confirmed_order_model){
        $currently_selected_order["suplier_id"] = 
        $currently_selected_order["order_price"] = $_POST["order_price"] ?? $confirmed_order_model->products->price;
        $currently_selected_order["order_amount"] = $_POST["order_amount"] ?? $confirmed_order_model->order_amount;
        $currently_selected_order["order_delivery"] = $_POST["order_delivery"] ?? $confirmed_order_model->products->delivery_time;
        $currently_selected_order["order_guaranty"] = $_POST["order_guaranty"] ?? $confirmed_order_model->products->guaranty;
        $currently_selected_order["order_rating"] = $_POST["order_rating"] ?? $confirmed_order_model->products->bidoSupliers->rating;
    }

    private function getCalculatedBidData($bid){
        $bid_calculations["savings_percent"] = self::getPercentageInValueIncrease($_POST["order_price"], $bid->bidData->price);
        $bid_calculations["total_savings_price"] = self::getValueDifference($_POST["order_price"], $bid->bidData->price, $_POST["order_amount"]);
        $bid_calculations["single_saving_price"] = self::getValueDifference($_POST["order_price"], $bid->bidData->price);
        $bid_calculations["delivery_percent"] = self::getPercentageInValueIncrease($_POST["order_delivery"], $bid->bidData->delivery_time);
        $bid_calculations["guaranty_percent"] = self::getPercentageInValueIncrease($_POST["order_guaranty"], $bid->bidData->guaranty);
        $bid_calculations["rating_percent"] = self::getPercentageInValueIncrease($_POST["order_rating"], $bid->user2suplier->bidoSupliers->rating);
        return $bid_calculations;
    }
    
    private function getPercentageInValueIncrease($previous_value, $new_value){
        $previous_value_percent = 100;
        $increase_in_value = (($new_value * $previous_value_percent)/$previous_value) - $previous_value_percent;
        return round($increase_in_value, 2 );;
    }
    
    private function getValueDifference($previous_value, $new_value, $amount = 1){
        $value_diff = ($new_value - $previous_value) * $amount;
        return round($value_diff,2);
    }
    
    private function bidComponent($bid_calculations, $bid_id){
        $bid_component = '<div class="row bid-deal">
            <input type="hidden" class="unique_bid" value="'. $bid->bid_id .'/>
            <div class="bid-button-discount">
                <h2 class="bid-price">'. $total_savings_percent .'%</h2>
                <p class="">
                    <strong>'. Yii::t("front_end", "Saving") . ": " .'</strong>'
                    . $total_savings_price . " eur" 
                .'</p>
            </div>
            <div class="bid-button-price">
                <h4 class="">
                    <strong>'. Yii::t("front_end", "Price") . ": " .'</strong>
                    <span>'. $bid["bidData"]["price"] .'</span>
                    <span style="color: #8EBF5F">(-20%)</span> 
                </h4>   
                <h4 class="">
                    <strong>'. Yii::t("front_end", "Suplier") . ": " .'</strong>
                    <span>'. $bid["user2suplier"]["bidoSupliers"]["title"] .'</span>
                </h4> 
            </div>
            <div class="bid-button-data">
                <p class="">
                    <strong>'. Yii::t("front_end", "Delivery") . ": " .'</strong>
                    <span>'. $bid["bidData"]["delivery_time"] .'</span>
                    <span style="color: #8EBF5F">(-20%)</span> 
                </p>
                <p class="">
                    <strong>'. Yii::t("front_end", "Guaranty") . ": " .'</strong>'
                    .$bid["bidData"]["guaranty"] .
                    '<span style="color: #8EBF5F">(-20%)</span> 
                </p> 
                <p class="">
                    <strong>'. Yii::t("front_end", "Rating") . ": " .'</strong>
                    <span>'. $bid["user2suplier"]["bidoSupliers"]["rating"] .'</span>
                    <span style="color: #8EBF5F">(-20%)</span> 
                </p> 
            </div>
        </div>';
    }
}
