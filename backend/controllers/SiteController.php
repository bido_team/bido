<?php
namespace backend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\models\SignupForm;
use common\models\Bids;
use common\models\ConfirmedOrders;
use common\models\CompletedBids;
use common\models\BidoSupliers;

/**
 * Site controller
 */
class SiteController extends Controller
{
    private $suplier_id;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout','view','create','update','delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout','view','create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $suplier_data = self::getSuplierData();
        self::__set("suplier_id",$suplier_data["id"]);
        
        //User data:
        $user_started_bids = self::getStartedBidsCount('user');
        $user_finished_bids = self::getCompletedBidsCount('user');
        
        //Suplier data:
        $suplier_started_bids = self::getStartedBidsCount();
        $suplier_finished_bids = self::getCompletedBidsCount();
        
        $available_bids = self::getAvailableBidsCount();
        
        return $this->render('index', [
            'user_started_bids' => ($user_started_bids)?$user_started_bids:0,
            'user_finished_bids' => ($user_finished_bids)?$user_finished_bids:0,
            'suplier_started_bids' => ($suplier_started_bids)?$suplier_started_bids:0,
            'suplier_finished_bids' => ($suplier_finished_bids)?$suplier_finished_bids:0,
            'available_bids' => ($available_bids)?$available_bids:0,
            'suplier_data' => $suplier_data
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->scenario = LoginForm::SCENARIO_SELLERS_LOGIN;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';
            $this->layout = "login";
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    private function getSuplierData(){
        $user_id = Yii::$app->user->identity->id;
        return BidoSupliers::getSuplierDataByUserId($user_id);
    }

    public function getStartedBidsCount($type = 'suplier'){
        switch ($type){
            case 'suplier':
                $suplier_id = self::__get("suplier_id");
                return Bids::countBidsBySuplierId($suplier_id);
                break;
            case 'user':
                $user_id = Yii::$app->user->identity->id;
                return Bids::countBidsByUserId($user_id);
                break;
            default:
                break;
        }
        
    }
    
    public function getAvailableBidsCount($type = 'suplier'){
        switch ($type){
            case 'suplier':
                $suplier_id = self::__get("suplier_id");
                return ConfirmedOrders::countAvailableBidsBySuplierId($suplier_id);
                break;
            case 'user':
                $user_id = Yii::$app->user->identity->id;
                return ConfirmedOrders::countAvailableBidsByUserId($user_id);
                break;
            default:
                break;
        }
    }
    
    public function getCompletedBidsCount($type = 'suplier'){
        switch ($type){
            case 'suplier':
                $suplier_id = self::__get("suplier_id");
                return CompletedBids::countCompletedBidsBySuplierId($suplier_id);
                break;
            case 'user':
                $user_id = Yii::$app->user->identity->id;
                return CompletedBids::countCompletedBidsByUserId($user_id);
                break;
            default:
                break;
        }
    }
    
    
}
